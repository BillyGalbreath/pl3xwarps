package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.Pl3xWarps;
import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.List;

public class CmdListWarps implements TabExecutor {
    private final Pl3xWarps plugin;

    public CmdListWarps(Pl3xWarps plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.listwarps")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        List<String> warps = WarpConfig.getConfig(plugin).getWarps();
        if (warps == null || warps.isEmpty()) {
            Lang.send(sender, Lang.WARP_LIST_EMPTY);
            return true;
        }

        Lang.send(sender, Lang.WARP_LIST
                .replace("{list}", String.join(", ", warps)));
        return true;
    }
}
