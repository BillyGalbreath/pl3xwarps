package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.Pl3xWarps;
import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdSetWarp implements TabExecutor {
    private final Pl3xWarps plugin;

    public CmdSetWarp(Pl3xWarps plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            return WarpConfig.getConfig(plugin).getMatchingWarps(args[0]);
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.setwarp")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length < 1) {
            Lang.send(sender, Lang.MUST_SPECIFY_WARP_NAME);
            return true;
        }

        String warpName = args[0].toLowerCase();

        WarpConfig.getConfig(plugin).setWarp(warpName, ((Player) sender).getLocation());

        Lang.send(sender, Lang.WARP_SET
                .replace("{warp}", warpName));
        return true;
    }
}
