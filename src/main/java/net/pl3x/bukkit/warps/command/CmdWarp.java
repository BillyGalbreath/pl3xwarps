package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.Pl3xWarps;
import net.pl3x.bukkit.warps.configuration.Config;
import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CmdWarp implements TabExecutor {
    private final Pl3xWarps plugin;

    public CmdWarp(Pl3xWarps plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            return WarpConfig.getConfig(plugin).getMatchingWarps(args[0].toLowerCase());
        }
        if (args.length == 2) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[1].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.warp")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length < 1) {
            Lang.send(sender, Lang.MUST_SPECIFY_WARP_NAME);
            return true;
        }

        String warpName = args[0].toLowerCase();
        Location warp = WarpConfig.getConfig(plugin).getWarp(warpName);

        if (warp == null) {
            Lang.send(sender, Lang.WARP_NOT_FOUND
                    .replace("{warp}", warpName));
            return true;
        }

        if (!sender.hasPermission("command.warp.*") &&
                !sender.hasPermission("command.warp." + warpName)) {
            Lang.send(sender, Lang.NO_WARP_PERM);
            return true;
        }

        Player player = (Player) sender;
        if (Config.USE_TELEPORT_SOUNDS) {
            player.getLocation().getWorld().playSound(player.getLocation(), Config.TELEPORT_SOUND, 1.0F, 1.0F);
            warp.getWorld().playSound(warp, Config.TELEPORT_SOUND, 1.0F, 1.0F);
        }

        player.teleport(warp);

        Lang.send(sender, Lang.WARP
                .replace("{warp}", warpName));
        return true;
    }
}
