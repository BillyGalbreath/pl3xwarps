package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.Pl3xWarps;
import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.List;

public class CmdDelWarp implements TabExecutor {
    private final Pl3xWarps plugin;

    public CmdDelWarp(Pl3xWarps plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            return WarpConfig.getConfig(plugin).getMatchingWarps(args[0].toLowerCase());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.delwarp")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length < 1) {
            Lang.send(sender, Lang.MUST_SPECIFY_WARP_NAME);
            return true;
        }

        WarpConfig warpConfig = WarpConfig.getConfig(plugin);
        String warpName = args[0].toLowerCase();
        Location warp = warpConfig.getWarp(warpName);

        if (warp == null) {
            Lang.send(sender, Lang.WARP_NOT_FOUND
                    .replace("{warp}", warpName));
            return true;
        }

        warpConfig.deleteWarp(warpName);
        Lang.send(sender, Lang.WARP_DELETED
                .replace("{warp}", warpName));
        return true;
    }
}
