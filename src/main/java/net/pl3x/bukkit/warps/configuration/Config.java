package net.pl3x.bukkit.warps.configuration;

import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Config {
    public static boolean COLOR_LOGS;
    public static boolean DEBUG_MODE;
    public static String LANGUAGE_FILE;
    public static boolean USE_TELEPORT_SOUNDS;
    public static Sound TELEPORT_SOUND;

    private Config() {
    }

    public static void reload(JavaPlugin plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        USE_TELEPORT_SOUNDS = config.getBoolean("use-teleport-sounds", true);
        try {
            TELEPORT_SOUND = Sound.valueOf(config.getString("teleport-sound", "ENTITY_ENDERMEN_TELEPORT"));
        } catch (IllegalArgumentException e) {
            TELEPORT_SOUND = Sound.ENTITY_ENDERMEN_TELEPORT;
        }
    }
}

