package net.pl3x.bukkit.warps.configuration;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION;
    public static String PLAYER_COMMAND;

    public static String WARP_NOT_FOUND;
    public static String WARP_EXEMPT;
    public static String MUST_SPECIFY_WARP_NAME;
    public static String WARP_LIST_EMPTY;
    public static String NO_WARP_PERM;
    public static String WARP_SET;
    public static String WARP_DELETED;
    public static String WARP;
    public static String WARP_OTHER;
    public static String WARP_TARGET;
    public static String WARP_LIST;

    public static String VERSION;
    public static String RELOAD;

    private Lang() {
    }

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        plugin.saveResource(Config.LANGUAGE_FILE, false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command");
        PLAYER_COMMAND = config.getString("player-command", "&4Player only command");

        WARP_NOT_FOUND = config.getString("warp-not-found", "&4Cannot find that warp!");
        WARP_EXEMPT = config.getString("warp-exempt", "&4You cannot warp that player!");
        MUST_SPECIFY_WARP_NAME = config.getString("must-specify-warp-name", "&4Must specify a warp name!");
        WARP_LIST_EMPTY = config.getString("warp-list-empty", "&4No warps are set!");
        NO_WARP_PERM = config.getString("no-warp-perm", "&4You do not have permission to use that warp!");
        WARP_SET = config.getString("warp-set", "&dWarp &7{warp} &dhas been set.");
        WARP_DELETED = config.getString("warp-deleted", "&dWarp &7{warp} &dhas been deleted.");
        WARP = config.getString("warp", "&dYou have warped to &7{warp}&d.");
        WARP_LIST = config.getString("warp-list", "&dWarps: {list}");

        VERSION = config.getString("version", "&d{plugin} v{version}");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
