package net.pl3x.bukkit.warps.configuration;

import net.pl3x.bukkit.warps.Pl3xWarps;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WarpConfig extends YamlConfiguration {
    private static WarpConfig config;

    public static WarpConfig getConfig(Pl3xWarps plugin) {
        if (config == null) {
            config = new WarpConfig(plugin);
        }
        return config;
    }

    private final File file;
    private final Object saveLock = new Object();

    private WarpConfig(Pl3xWarps plugin) {
        super();
        file = new File(plugin.getDataFolder(), "warps.yml");
        load();
    }

    private void load() {
        synchronized (saveLock) {
            try {
                this.load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    public List<String> getWarps() {
        if (!isSet("warps")) {
            return new ArrayList<>();
        }
        List<String> warps = getConfigurationSection("warps").getValues(false).keySet().stream()
                .map(String::toLowerCase).collect(Collectors.toList());
        Collections.sort(warps);
        return warps;
    }

    public List<String> getMatchingWarps(String name) {
        return getWarps().stream()
                .filter(warp -> warp.toLowerCase().startsWith(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    public Location getWarp(String warp) {
        if (get("warps." + warp) == null) {
            return null;
        }
        World world = Bukkit.getWorld(getString("warps." + warp + ".world", ""));
        if (world == null) {
            return null;
        }
        double x = getDouble("warps." + warp + ".x");
        double y = getDouble("warps." + warp + ".y");
        double z = getDouble("warps." + warp + ".z");
        float pitch = (float) getDouble("warps." + warp + ".pitch");
        float yaw = (float) getDouble("warps." + warp + ".yaw");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public void setWarp(String warp, Location location) {
        set("warps." + warp + ".x", location.getX());
        set("warps." + warp + ".y", location.getY());
        set("warps." + warp + ".z", location.getZ());
        set("warps." + warp + ".world", location.getWorld().getName());
        set("warps." + warp + ".pitch", location.getPitch());
        set("warps." + warp + ".yaw", location.getYaw());
        save();
    }

    public void deleteWarp(String warp) {
        set("warps." + warp, null);
        save();
    }
}
