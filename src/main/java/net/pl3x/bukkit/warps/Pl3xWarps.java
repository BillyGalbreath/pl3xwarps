package net.pl3x.bukkit.warps;

import net.pl3x.bukkit.warps.command.CmdDelWarp;
import net.pl3x.bukkit.warps.command.CmdListWarps;
import net.pl3x.bukkit.warps.command.CmdPl3xWarps;
import net.pl3x.bukkit.warps.command.CmdSetWarp;
import net.pl3x.bukkit.warps.command.CmdWarp;
import net.pl3x.bukkit.warps.configuration.Config;
import net.pl3x.bukkit.warps.configuration.Lang;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xWarps extends JavaPlugin {
    private final Logger logger;

    public Pl3xWarps() {
        logger = new Logger(this);
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        getCommand("delwarp").setExecutor(new CmdDelWarp(this));
        getCommand("listwarps").setExecutor(new CmdListWarps(this));
        getCommand("setwarp").setExecutor(new CmdSetWarp(this));
        getCommand("warp").setExecutor(new CmdWarp(this));
        getCommand("pl3xwarps").setExecutor(new CmdPl3xWarps(this));

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        getLog().info(getName() + " disabled.");
    }
}
